package com.atk.visitor;

/**
 * @author Adrian Kurz on 03.08.21.
 * @project visitorcounter
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages={"com.atk.visitor"}) //added specific scan of VisitorService because otherwise application won't start
public class VisitorCounterApplication {

	public static void main(String[] args) {

		SpringApplication.run(VisitorCounterApplication.class, args);
	}
}