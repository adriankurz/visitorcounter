# Visitor Counter
Visitor Counter is a webapp made with Spring Boot and Java 11, using Spring MVC, Spring Data JPA, Hibernate, ThymeLeaf, Bootstrap and H2 in-memory database.
## Build the app locally

This is targeted at people without Maven experience.

To build it, you will need to download and unpack the latest (or recent) version of [Maven](https://maven.apache.org/download.cgi) and put the `mvn` command on your path. Then, you will need to install a Java 11 (or higher) JDK (not JRE!), and make sure you can run `java` from the command line. Now you can run `mvn clean install` and Maven will compile your project, and put the results it in a jar file in the `target` directory.

How you run this code is up to you, but usually you would start by using an IDE like [Intellij IDEA](https://www.jetbrains.com/idea/), [Eclipse](https://eclipse.org/ide/) or [NetBeans](https://netbeans.org/).

The Maven dependencies may lag behind the official releases a bit.

A couple of Maven commands
Once you have configured your project in your IDE you can build it from there. However, if you prefer you can use maven from the command line. In that case you could be interested in this short list of commands:

* `mvn compile`: it will just compile the code of your application and tell you if there are errors
* `mvn test`: it will compile the code of your application and your tests. It will then run your tests (if you wrote any) and let you know if some fails
* `mvn install`: it will do everything `mvn test` does and then if everything looks file it will install the library or the application into your local maven repository (typically under /.m2). In this way you could use this library from other projects you want to build on the same machine

If you need more information please take a look at this [quick tutorial](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html).

## Run the app
`mvn spring-boot:run`

## Accesing the app
After the app is running you can access it by entering `localhost:8080` in your browser

## Usage
Every click on "Call Backend" button should add a new line to the table containing a User ID, the user agent and the date it was created.

The total number of persisted users should update after each call

By clicking on the "delete" button you can delete entries line by line

Visitor count will be updated after each delete operation
