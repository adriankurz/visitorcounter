package com.atk.visitor.model;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table(name ="visitors")
public class Visitor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "access_date")
    private LocalDateTime accessDate;

    @Column(name = "user_agent")
    private String userAgentInfo;

    public Long getId() {
        return id;
    }

    public LocalDateTime getAccessDate() {
        return accessDate;
    }

    public void setAccessDate(LocalDateTime accessDate) {
        this.accessDate = accessDate;
    }

    public String getUserAgentInfo() {
        return userAgentInfo;
    }

    public void setUserAgentInfo(String userAgentInfo) {
        this.userAgentInfo = userAgentInfo;
    }
}
