package com.atk.visitor.service;

/**
 * @author Adrian Kurz on 02.08.21.
 * @project visitorcounter
 */

import com.atk.visitor.model.Visitor;

import java.util.List;

public interface VisitorService {

    List<Visitor> getAllVisitors();

    void saveVisitor(Visitor visitor);

    void deleteVisitorById(Long id);
}
