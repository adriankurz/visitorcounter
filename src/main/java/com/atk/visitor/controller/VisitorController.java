package com.atk.visitor.controller;

import com.atk.visitor.model.Visitor;
import com.atk.visitor.service.VisitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Adrian Kurz on 08.08.21.
 * @project counter
 */

@Controller
public class VisitorController {

    @Autowired
    private VisitorService visitorService;

    @GetMapping("/")
    public String viewHomePage(Model model) {
        model.addAttribute("listVisitors", visitorService.getAllVisitors());
        return "index";
    }

    @Autowired
    private HttpServletRequest servletRequest;

    @RequestMapping(value="/save", method = RequestMethod.POST)
    public String saveNewVisitor(@ModelAttribute("saveVisitor") Visitor visitor ) {
        visitor.setUserAgentInfo(servletRequest.getHeader("user-agent"));
        visitor.setAccessDate(LocalDateTime.now());
        visitorService.saveVisitor(visitor);
        return "redirect:/";
    }

    @GetMapping("/deleteVisitor/{id}")
    public String deleteVisitor(@PathVariable (value = "id") Long id) {
        visitorService.deleteVisitorById(id);
        return "redirect:/";
    }
}