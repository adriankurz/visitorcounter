package com.atk.visitor.repository;

/**
 * @author Adrian Kurz on 02.08.21.
 * @project visitorcounter
 */

import com.atk.visitor.model.Visitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VisitorRepository extends JpaRepository<Visitor, Long> {

}
