package com.atk.visitor.service;

/**
 * @author Adrian Kurz on 03.08.21.
 * @project visitorcounter
 */

import com.atk.visitor.model.Visitor;
import com.atk.visitor.repository.VisitorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VisitorServiceImpl implements VisitorService {

    @Autowired
    private VisitorRepository visitorRepository;

    @Override
    public List<Visitor> getAllVisitors() {
        return visitorRepository.findAll();
    }

    @Override
    public void saveVisitor(Visitor visitor) {
        visitorRepository.save(visitor);
    }

    @Override
    public void deleteVisitorById(Long id) {
        visitorRepository.deleteById(id);
    }
}
